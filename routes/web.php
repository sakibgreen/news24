<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//news
Route::get('/','NewsController@index')->name('news.index');
Route::get('create','NewsController@create')->name('news.create');
Route::post('news','NewsController@store')->name('news.store');
Route::get('news/{id}/show','NewsController@show')->name('news.show');
Route::get('/manage', 'NewsController@manage')->name('news.manage');
Route::get('news/{news}/edit', 'NewsController@edit')->name('news.edit');
Route::post('news/{id}', 'NewsController@update')->name('news.update');
Route::delete('news/{news}', 'NewsController@destroy')->name('news.destroy');

//comment 
Route::post('/news/{news}/comment','CommentController@store')->name('comments.store');
Route::get('/news/{news}/comment','CommentController@show')->name('comments.show');
Route::get('comments/{comment}/edit', 'CommentController@edit')->name('comments.edit');
Route::post('comments/{id}', 'CommentController@update')->name('comments.update');
Route::delete('comment/{comment}', 'CommentController@destroy')->name('comments.destroy');

//Reply
Route::post('/news/{news}/comments/{comment}/reply','ReplyController@store')->name('reply.store');
Route::get('/news/{news}/comments/{comment}/reply','ReplyController@show')->name('replies.show');
Route::delete('reply/{reply}', 'ReplyController@destroy')->name('reply.destroy');
Route::get('reply/{reply}/edit', 'ReplyController@edit')->name('reply.edit');
Route::put('/news/{news}/comments/{comment}/reply/{id}', 'ReplyController@update')->name('reply.update');
