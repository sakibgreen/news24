@extends('layouts.app')

@section('content')
	
	<div class="container">
		<div class="row">
			<form class="form-horizontal mt5" action="{{route('comments.update',['id'=>$comments->id])}}" method="post">
        	{{csrf_field()}}
          		<fieldset>
		            <!-- Text input-->
		            <div class="form-group">
		              <label class="col-md-4 control-label" for="">Comment</label>  
		              <div class="col-md-5">
		              <input id="comment" name="comment" type="text" placeholder="write your comment" class="form-control input-md" required="" value="{{$comments->comments}}">
		              
		              </div>
		            </div>

		            <!-- Button -->
		            <div class="form-group">
		              <label class="col-md-4 control-label" for="button"></label>
		              <div class="col-md-4">
		                <button id="button" class="btn btn-inverse">Save</button>
		              </div> 
		            </div>

          		</fieldset>
     		 </form>
			
		</div>
	</div>

@endsection   