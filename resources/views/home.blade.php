@extends('layouts.app')

@section('content')

    <div class="content">
		<nav class="navbar navbar-dark bg-primary">
			<div class="links">
	        	<a href="{{route('news.manage')}}" class="btn btn-primary btn-lg btn-block">Manage Post</a>
	            <a href="{{ route('news.create') }}" class="btn btn-primary btn-lg btn-block">Add Post</a>
	        </div>
		</nav>
    </div>

@endsection

