@extends('layouts.app')

@section('content')

<div class="container">
    <!--Post Show-->
    <div class="row">        
        <div class="col-sm-12">
            <div class="card text-center">
                <div class="card-header">
                    <h4>Post ID :{{$news->id}}</h4>
                    <h5>Posted By: {{ App\News::user_name($news->user_id) }}</h5>
                </div>
                <div class="card-block">
                    <h4 class="card-title">
                      <tr>
                    <h3><td>{{ $news->title }} </td></h3>
                  </tr></h4>
                  <tr>
                      <td><img src="{{ asset("storage/upload/" .$news->image_name)}}" width="500"></td>
                  </tr>
                    <p class="card-text">
                      <tr>
                    <td>{{ $news->body }}</td>
                </tr></p>
                </div>
            </div> 
        </div>
    </div>
    <br><br>
    <!--Comment input-->
    <form class="form-horizontal mt5" action="/news/{{$news->id}}/comment" method="post">
        {{csrf_field()}}
          <fieldset>
            <!-- Text input-->
            <div class="titleBox">
              <label>Comment Box</label>
            </div>
            <div class="form-group">
                <input id="comment" name="comment" class="form-control" type="text" placeholder="Your comments" />
            </div>
            <div class="form-group">
                <button class="btn btn-default btn-sm">Add</button>
            </div>

          </fieldset>
                

      </form>
      <!--Show comments-->
      @foreach ($comments as $comment)
      
          <ul> 
              <div class="card-comment-show">
                <div class="card text">
                  <h5><b>Comment:</b> {{ App\News::user_name($comment->user_id) }} : {{$comment->comments}}</h5>   
                </div>       

                  @if ($comment->user_id == auth()->user()->id)
                    <form action="{{route('comments.destroy',$comment->id)}}" method="post">
                      {{csrf_field()}}
                        <a href="{{route('comments.edit', $comment->id)}}" class="btn btn-default btn-sm">Edit</a>
                        <input type="hidden" name="_method" value="DELETE">
                        <button class="btn btn-default btn-sm">Delete</button>                 
                    </form> 
                  @endif
              </div>

                <!--reply show-->

                @foreach( $reply as $r)
                  @if($r->comment_id == $comment->id)
                    <ul>
                        <div class="card-reply-show">
                              <div class="card text">
                                <h5><b>Reply:</b> {{ App\News::user_name($r->user_id) }}: {{ $r->replies }} </h5>
                              </div>
                            @if ($r->user_id == auth()->user()->id)
                              <form action="{{route('reply.destroy',$r->id)}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-default btn-sm">Delete</button>
                                <a href="{{route('reply.edit', $r->id)}}" class="btn btn-default btn-sm">Edit</a>
                              </form>
                            @endif 
                            @if($comment->user_id == auth()->user()->id)
                              <form action="{{route('reply.destroy',$r->id)}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-default btn-sm">Delete</button>
                              </form> 
                            @endif 
                        </div>  
                    </ul>
                  @endif
                @endforeach
                 <!--Reply Comment-->
                <form class="form-horizontal mt5" action="/news/{{$news->id}}/comments/{{$comment->id}}/reply" method="post">
                    {{csrf_field()}}
                      <fieldset>
                          <!-- Text input-->
                          <div class="titleBox">
                            <label>Reply Box</label>
                          </div>
                          <div class="form-group">
                            <input id="reply" name="replies" class="form-control" type="text" placeholder="reply comments" />
                          </div>
                          <div class="form-group">
                            <button class="btn btn-default btn-sm">Add</button>
                          </div>
                      </fieldset>
                </form>      
          </ul>  
      @endforeach

</div>
 
@endsection
