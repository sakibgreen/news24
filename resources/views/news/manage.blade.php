@extends('layouts.app')

@section('content')

			<h1><center>All Post</center></h1>
				<table class="table table-dark">
				  <thead>
				    <tr>
				      <th scope="col">Title</th>
				      <th scope="col">image</th>
				      <th scope="col">Body</th>	 
				      <th scope="col">Action</th>
				    </tr>
				  </thead>
				  <tbody>
				  	@foreach($news as $post)
				  		@if ($post->user_id == auth()->user()->id)
					  		<tr>
					  			<td>{{ $post->title }}</td>
					  		
					  			<td><img src="{{ asset("storage/upload/".$post->image_name)}}" width="200"></td>
					  			<td>{{ $post->body }}</td>
					  			<td><a href="{{route('news.edit', $post->id)}}" class="btn btn-warning">Edit</a></td>
								<td><form action="{{route('news.destroy',$post->id)}}" method="post">
									{{csrf_field()}}
									<input type="hidden" name="_method" value="DELETE">
									<button class="btn btn-danger btn-sm">Delete</button>
									</form>
						  		</td>

					  		</tr>
					  	@endif
				  	@endforeach
				  </tbody>
				</table>

@endsection