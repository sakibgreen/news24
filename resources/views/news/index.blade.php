@extends('layouts.app')

@section('content')

<div class="container">
		<div class="col-lg-12">
			<img class="card-img-top" src="{{ asset("sakib_cover/cover.jpg")}}" width="100%" height="350" alt="Card image cap">
		</div>
		<div class="row marketing">
			@foreach($all_posts as $post)

				<div class="col-lg-6">
					
					<h5 class="card-post-id">Post ID: {{$post->id}}</h5>

		  			<img class="card-img-top" src="{{ asset("storage/upload/".$post->image_name)}}" width="100%" height="350" alt="Card image cap">
			  		<div class="card-body">
			    		<h5 class="card-title">{{ $post->title}}</h5>
			    		<p class="card-text">{{ str_limit($post->body, 100) }}</p>
			    		<a href="{{route('news.show', $post->id)}}" >Read More..</a>
				  	</div>
				</div>  	 		
			@endforeach
		</div>
	
</div>

@endsection

