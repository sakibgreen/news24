<?php

namespace App;
use App\User;
use App\Reply;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $guarded=[];

    public function comments()
    {
    	return $this->hasMany(Comment::class);
    }

    public function users()
    {
    	return $this->belongsTo(User::class);
    }
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public static function user_name($id)
    {
    	$name = User::find($id);
    	if($name != NULL)
    	{
    		return $name->name;
    	}
    	else
    	{
    		return 'Fuck Yourself';
    	}
    }

    public static function comment_replies($id)
    {
        $replies = Reply::where('comment_id',$id)->get();
        if(@$replies != NULL)
        {
            return $replies->replies;
        }

    }
}
