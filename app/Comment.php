<?php

namespace App;
use App\News;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $guarded=[];

	public function news()
	{
		return $this->belongsTo(News::class);
	}

	public function users()
	{
		return $this->belongsTo(User::class);
	}
	public function replies()
    {
        return $this->hasMany(Reply::class);
    }
}
