<?php

namespace App\Http\Controllers;
use App\Comment;
use App\News;
use App\User;
use App\Reply;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, News $news)
    {
        // dd($request->comment);
        $comments = new Comment;
        $comments->comments = $request->comment;
        $comments->user_id  = auth()->user()->id;
        $comments->post_id  = $news->id;
        $comments->save();

        return back();        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comments = Comment::find($id);
        return view('comments.edit',compact('comments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment            = Comment::find($id);
        $post_id            = $comment->post_id;
        $comment->comments  = $request->comment;
        $comment->post_id   = $post_id;
        $comment->save();

        return redirect()->route('news.show',$post_id); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comments =  Comment::find($id);
        // dd($comments);
        $comments->delete();

        return back(); 
    }
}
   