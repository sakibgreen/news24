<?php

namespace App\Http\Controllers;
use App\Reply;
use App\Comment;
use App\News;
use App\User;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    public function store(Request $request,News $news ,Comment $comment)
    {
    	$replies = new Reply;
    	$replies->replies   	= $request->replies;
    	$replies->user_id		= auth()->user()->id;
    	$replies->comment_id	= $comment->id;
    	$replies->news_id   	= $news->id;
    	// dd($replies);
    	$replies->save();

    	return back();
    }

    public function destroy($id)
    {
        $replies = Reply::find($id);
        // dd($replies);
        $replies->delete();

        return back();
    }

    public function edit($id)
    {
        $reply = Reply::find($id);
        $reply_comment_id   = $reply->comment_id;
        $reply_news_id      = $reply->news_id;
        return view ('reply.edit',compact('reply','reply_comment_id','reply_news_id'));
    }

    public function update(Request $request, News $news ,Comment $comment, $id)
    {
        $reply                = Reply::find($id);
        $news_id              = $reply->news_id;
        $reply->replies       = $request->reply;
        $reply->user_id       = auth()->user()->id;
        $reply->comment_id    = $comment->id;
        $reply->news_id       = $news->id;
        // dd($replies);
        $reply->save();

        return redirect()->route('news.show',$news_id);
    }
}
 