<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\News;
use App\Comment;
use App\Reply;



class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => 'index']);
    }
    public function index()
    {
        $all_posts = News::all();
        return view('news.index',compact('all_posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('image_name'))
        {
            $image_name = $request->image_name->getClientOriginalName();            
            $image_size = $request->image_name->getClientSize()/1024/1024;
            $request->image_name->storeAs('public/upload',$image_name);            

            $news = new News;
            $news->title        = $request->title;
            $news->user_id      = auth()->user()->id;
            $news->image_name   = $image_name;
            $news->image_size   = $image_size;
            $news->body         = $request->body;
            $news->save(); 

        }  

        return redirect()->route('news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news               = News::find($id);
        $news_id            = $news->id;
        $comments           = Comment::where('post_id',$news_id)->get();
        $reply              = Reply::where('news_id',$news_id)->get();

        return view ('news.show',compact('news','comments','reply')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function manage()
    {
        $news = News::all();
        return view('news.manage',compact('news'));
    }

    public function edit($id)
    {
        $news = News::find($id);
        return view ('news.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        if($request->hasFile('image_name'))
        {
            $image_name = $request->image_name->getClientOriginalName();            
            $image_size = $request->image_name->getClientSize()/1024/1024;
            $request->image_name->storeAs('public/upload',$image_name);    

            $news = News::find($id);
            $news->update([
                'title'         =>  request('title'),
                'image_name'    =>  $image_name,
                'image_size'    =>  $image_size,
                'body'          =>  request('body')
            ]);
        }
         return redirect()->route('news.manage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);
        $news->delete();
        return redirect()->route('news.manage');
    }
}
   