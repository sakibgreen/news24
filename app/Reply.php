<?php

namespace App;
use App\Comment;
use App\News;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $guarded=[];

	public function comments()
	{
		return $this->belongsTo(Comment::class);
	}
	public function news()
	{
		return $this->belongsTo(News::class);
	}
	public function users()
	{
		return $this->belongsTo(User::class);
	}

}
 