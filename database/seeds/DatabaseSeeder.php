<?php

use Illuminate\Database\Seeder;
use App\User;
use App\News;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class,10);
    	User::truncate();
        factory(User::class,10)->create();

    }
}
